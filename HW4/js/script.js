// 1. Описати своїми словами навіщо потрібні функції у програмуванні.
//    Функції потрібні щоб виконувати однакові дії багато разів в будь-якй частині програми.
// 2. Описати своїми словами, навіщо у функцію передавати аргумент.
//     Це таке правило запису в js, при оголошенні функції ми викоростовуємо параметри,
//     а при виклику функції ми передаємо вже аргументи.
// 3. Що таке оператор return та як він працює всередині функції?
//     Це значення яке повертає функція у якості результату.
//     Return може бути в будь-якій частині функції, може бути декілька разів та може
//     бути без значення. Якщо return без значення це означає вихід з функції.

"use strict";
let num1 = +prompt("Enter number 1");
let num2 = +prompt("Enter number 2");
while (!num1 || !num2) {
    num1 = +prompt("Enter number 1 again", num1);
    num2 = +prompt("Enter number 2 again", num2);
}
let sign = prompt("Enter the operation: +, -, *, /");

function calc(num1, num2, sign) {
    if (sign === "+") {
        return num1 + num2;
    } else if (sign === "-") {
        return num1 - num2;
    } else if (sign === "*") {
        return num1 * num2;
    } else if (sign === "/") {
        return num1 / num2;
    }
}

console.log(calc(num1, num2, sign));