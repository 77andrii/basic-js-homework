// 1. Які існують типи даних у Javascript?
//     Основні типи данних: number (числа), bigint (числа довільної довжини), string (строки),
//     Boolean (true або false), null (невідоме значення), undefined (не призначене значення),
//     object (об’єкти), symbol (символи).
// 2. У чому різниця між == і ===?
//     == це оператор порівняння дорівнює. Порівнює значення без урахування типу данних;
//     === це оператор порівнянн строго дорівнює. Порівнює значення з урахуванням типу данних.
// 3. Що таке оператор?
//     Оператор це знак який виконує операції над операндом або операндами.
//     Основні оператори:
// -	присвоювання, наприклад =; +=, -=;
// -	порівняння, наприклад ==, ===, !=;
// -	арифметичні, наприклад +, -, *,/,%,** );
// -	бітові, наприклад &, |, ^;
// -	логічні оператори &&, ||;
// -	унарні та бінарні.

"use strict";
let name = prompt("Enter your name");
let age = + prompt("Enter your age");
while (!name || !age || age <= 0 ) {
   name = prompt("Enter your name again", name);
   age = prompt("Enter your age again", age);
}
if (age < 18) {
    alert('You are not allowed to visit this website');
} else if (age >= 18 && age <= 22) {
    let agree = confirm("Are you sure you want to continue?");
    if (agree) {
        alert('Welcome, ' + name)
    } else {
        alert('You are not allowed to visit this website')
    }
} else if (age > 22) {
    alert('Welcome, ' + name)
}
